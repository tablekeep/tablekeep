import os
from django.conf.global_settings import *

SECRET_KEY = 'ckx08f(qb79z(%@g(^1ni0w8x*bzdotmu65jk8j$ftlr%=8c&k'

DEBUG = False

ALLOWED_HOSTS = ['tablekeep.ro', ]

LOGIN_URL = '/login/'
LOGIN_REDIRECT_URL = '/'


INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.humanize',

    'compressor',
    'swampdragon',

    'tablekeep.apps.base',
    'tablekeep.apps.locations',
)

# swampdragon settings
SWAMP_DRAGON_CONNECTION = ('swampdragon.connections.sockjs_connection.DjangoSubscriberConnection', '/data')

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'django.middleware.security.SecurityMiddleware',
)

ROOT_URLCONF = 'tablekeep.apps.base.urls'

# TEMPLATES = [
#     {
#         'BACKEND': 'django.template.backends.django.DjangoTemplates',
#         'DIRS': [],
#         'APP_DIRS': True,
#         'OPTIONS': {
#             'context_processors': [
#                 'django.template.context_processors.debug',
#                 'django.template.context_processors.request',
#                 'django.contrib.auth.context_processors.auth',
#                 'django.contrib.messages.context_processors.messages',
#             ],
#         },
#     },
# ]


WSGI_APPLICATION = 'tablekeep.wsgi.application'

settings_file = os.path.realpath(__file__)
PROJECT_ROOT = os.path.dirname(os.path.dirname(os.path.dirname(settings_file)))

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(PROJECT_ROOT, 'db.sqlite3'),
    }
}

# Internationalization
# https://docs.djangoproject.com/en/1.8/topics/i18n/

LANGUAGE_CODE = 'ro'

# LOCALE_PATHS = (
#     os.path.join(PROJECT_ROOT, 'locale'),
# )

TIME_ZONE = 'Europe/Bucharest'

USE_I18N = True

USE_L10N = True

# USE_TZ = True
DATETIME_INPUT_FORMAT = (
    '%H:%M %d-%b-%Y',
)
DATETIME_INPUT_FORMATS = DATETIME_INPUT_FORMAT + DATETIME_INPUT_FORMATS


###############################################################################
# Templates
###############################################################################
TEMPLATE_CONTEXT_PROCESSORS += (
    'django.core.context_processors.request',
)

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.8/howto/static-files/

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.6/howto/static-files/
###############################################################################
# Build PROJECT_ROOT, MEDIA and STATIC
###############################################################################
MEDIA_ROOT = '%s/tablekeep/uploads/' % PROJECT_ROOT
MEDIA_URL = '/uploads/'
STATIC_ROOT = '%s/tablekeep/sitestatic/' % PROJECT_ROOT
STATIC_URL = '/static/'
ADMIN_MEDIA_PREFIX = '/static/admin/'
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
    'compressor.finders.CompressorFinder',
)

###############################################################################
# Logging
###############################################################################
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'simple': {
            'format': '%(levelname)s %(asctime)s %(pathname)s :%(lineno)d - %(message)s',
            'datefmt': '%d-%m-%Y %H:%M:%S',
        },
    },
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        }
    },
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler'
        },
        'tablekeep': {
            'level': 'INFO',  # minimum capture
            'class': 'logging.FileHandler',
            'formatter': 'simple',
            'filename': os.path.join(PROJECT_ROOT, 'logs', 'tablekeep.log')
        }
    },
    'loggers': {
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': True,
        },
        'tablekeep': {
            'handlers': ['tablekeep', ],
            'level': 'INFO',
        }
    },
}

##############################################################################
LOCATIONS_BOOKING_GAP = 60 * 60   # in seconds

from .base import *

DRAGON_URL = 'http://localhost:9999/'

# Above import enough for production environment

# DEBUG = True
# TEMPLATE_DEBUG = DEBUG
# THUMBNAIL_DEBUG = DEBUG
# 
# if DEBUG:
#     GOOGLE_ANALYTICS_KEY = ''
#     ALLOWED_HOSTS += ['127.0.0.1', '127.1.1.1']
#     DTABASES = {
#         'default': {
#             'ENGINE': 'django.db.backends.sqlite3',
#             'NAME': os.path.join(PROJECT_ROOT, 'db.sqlite3'),
#             'USER': '',
#             'PASSWORD': '',
#             'HOST': '',
#             'PORT': '',
#         }
#     }
# 
#     # COMPRESS_ENABLED = True
# 
#     # DEBUG TOOLBAR
#     INSTALLED_APPS += (
#         'debug_toolbar',
#     )
#     # By default this is the same as DEBUG - make it False
#     DEBUG_TOOLBAR_PATCH_SETTINGS = False
#     MIDDLEWARE_CLASSES += (
#         'debug_toolbar.middleware.DebugToolbarMiddleware',
#     )
# 
#     def show_toolbar(request):
#         return False
#         return True
# 
#     DEBUG_TOOLBAR_CONFIG = {
#         # 'INTERCEPT_REDIRECTS': False,
#         'SHOW_TOOLBAR_CALLBACK': 'tablekeep.settings.show_toolbar',
#         'SHOW_COLLAPSED': True,
#     }
#     DEBUG_TOOLBAR_PANELS = [
#         'debug_toolbar.panels.versions.VersionsPanel',
#         'debug_toolbar.panels.timer.TimerPanel',
#         'debug_toolbar.panels.settings.SettingsPanel',
#         'debug_toolbar.panels.headers.HeadersPanel',
#         'debug_toolbar.panels.request.RequestPanel',
#         'debug_toolbar.panels.sql.SQLPanel',
#         'debug_toolbar.panels.staticfiles.StaticFilesPanel',
#         'debug_toolbar.panels.templates.TemplatesPanel',
#         'debug_toolbar.panels.cache.CachePanel',
#         'debug_toolbar.panels.signals.SignalsPanel',
#         'debug_toolbar.panels.logging.LoggingPanel',
#         'debug_toolbar.panels.redirects.RedirectsPanel',
#     ]
#     # END DEBUG TOOLBAR

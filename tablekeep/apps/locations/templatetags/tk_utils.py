import re
from django import template


register = template.Library()


@register.filter
def get_key(dictionary, key):
    if not isinstance(dictionary, dict):
        return None
    return dictionary.get(key)


@register.filter
def number_to_month(number_string):

    if number_string.startswith('0'):
        number_string = number_string[1:]

    number_to_month_map = {
        '1': 'Ianuarie',
        '2': 'Februarie',
        '3': 'Martie',
        '4': 'Aprilie',
        '5': 'Mai',
        '6': 'Iunie',
        '7': 'Iulie',
        '8': 'August',
        '9': 'Septembrie',
        '10': 'Octombrie',
        '11': 'Noiembrie',
        '12': 'Decembrie'
    }
    return number_to_month_map.get(number_string, None)


@register.filter
def replace(string, args):
    bits = args.split("|")
    pattern = bits[0]

    try:
        replace_with = bits[1]
    except IndexError:
        # just remove if nothing provided
        replace_with = ''

    if replace_with == 'space':  # hardcoded value to replace with spaces
        replace_with = ' '

    return re.sub(pattern, replace_with, string)

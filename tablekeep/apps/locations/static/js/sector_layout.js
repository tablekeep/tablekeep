SectorLayout = {
    init: function(){
        Utils.make_draggable("#tablesWrap .tbl", "#tablesWrap");
        //Utils.make_resizable("#tablesWrap .tbl", "#tablesWrap");

        this.init_ui();
        this.handle_sector_layout();
        this.handle_table_ops();
    },
    init_ui: function(){
        // set width and height to keep a 16/9 aspect ratio
        var container = $("#tablesWrap");
        var width = container.width(); // set by css
        var height = width * 9 / 16; // maintain a 16/9 aspect ratio
        container.css({
            height: height,
        });

        $(".tbl").each(function(index, item){
            var tbl = $(item);
            Utils.prettify_tbl_number(tbl);
        });
    },
    handle_sector_layout: function(){
        // save the sector layout then show the mini sectors
        $("body").on("click", ".sector-layout-save", function(event){
            event.preventDefault();
            var self = $(this);
            // gather data about each table from tablesWrap
            var container = $("#tablesWrap");
            var table_data = [];
            container.find(".tbl").each(function(index, item){
                var tbl = $(item);
                var data = {
                    id: tbl.data("id"),
                    style: tbl.attr("style"),
                }
                table_data.push(data)
            });

            if (!self.hasClass("saving")){
                self.addClass("saving");
                $.ajax({
                    url: self.data("href"),
                    method: "POST",
                    data: $.param({
                        csrfmiddlewaretoken: $("input[name='csrfmiddlewaretoken']").val(),
                        layout: JSON.stringify(table_data)
                    }),
                    success: function(response){
                        location.href = response;
                    }   
                });
            }
        });
    },
    handle_table_ops: function(){
        // add remove chairs
        $("body").on("click", ".tbl .chair-opts", function(event){
            event.preventDefault();
            var self = $(this);
            var container = self.closest(".tbl");
            var chair_count = container.find(".chair-count").text();
            var chairs_class = "chairs-" + chair_count.trim();

            if (!self.hasClass("sending")){
                self.addClass("sending");
                $.ajax({
                    url: self.attr("href"),
                    method: "POST",
                    data: {
                        csrfmiddlewaretoken: $("input[name='csrfmiddlewaretoken']").val(),
                    },
                    success: function(response){
                        self.removeClass("sending");
                        container.find(".chair-count").text(response);
                        container.css({height: '', width: ''});
                        container.removeClass(chairs_class).addClass("chairs-" + response);
                    },
                    error: function(response){
                        // we might get 404 when removing chairs then the table has one chair
                        self.removeClass("sending");
                    }
                });
            } 
        });

        // rotate table
        $("body").on("click", ".tbl .rotate-table", function(event){
            event.preventDefault();
            var self = $(this);
            var tbl = self.closest(".tbl");
            var width = parseFloat(tbl.css("width"));
            var height = parseFloat(tbl.css("height"));

            tbl.css({
                height: width,
                width: height
            });

            // we need the css inline
            if (!tbl.hasClass("rotated")){
            } else {
            }

            // simulate drag so width and height gets recomputed in percent from draggable stop function
            tbl.simulate("drag");
            
            // rearrange the number
            Utils.prettify_tbl_number(tbl);
        });

        // create table through ajax pulled forms
        Utils.handle_ajax_modals();

        // delete table
        $("body").on("click", ".delete-table", function(event){
            event.preventDefault();
            var self = $(this);

            if (!self.hasClass("sending")){
                self.addClass("sending");
                $.ajax({
                    url: self.attr("href"),
                    method: "POST",
                    data: {
                        csrfmiddlewaretoken: $("input[name='csrfmiddlewaretoken']").val(),
                    },
                    success: function(response){
                        self.closest(".tbl").remove();
                    },
                    error: function(response){
                        self.removeClass("sending");
                    }
                });
            }
        });
    },
}
$(function(){
    SectorLayout.init();
    $(window).on("resize", function(){
        SectorLayout.init_ui();
    });
});

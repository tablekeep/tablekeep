SectorDetail = {
    init: function(){
        Utils.init_closers();
        this.scale_sector_canvas();
        this.handle_tbl();
        this.handle_tbl_drag_n_drop();
        this.call_the_dragons();
        this.tbl_opts_handle_exit();
        this.handle_confirm_drop();  // live event
        this.handle_hashed_scroll();
    },
    scale_sector_canvas: function(){
        // set width and height to keep a 16/9 aspect ratio
        var container = $(".tablesWrap");
        var width = container.width(); // set by css
        var height = width * 9 / 16; // maintain a 16/9 aspect ratio

        container.css({
            height: height,
        });
        container.find(".slideOpts div.left").css("padding-top", height / 2);
        container.find(".slideOpts div.right").css("padding-top", height / 2);
        this.prettify_tbl_all();
    },
    prettify_tbl_all: function(){
        var that = this;
        $(".tablesWrap .tbl").each(function(index, item){
            that.prettify_tbl($(item));
        });
    },
    prettify_tbl: function(tbl){
        this.prettify_tbl_chairs(tbl);
        Utils.prettify_tbl_number(tbl);
    },
    prettify_tbl_chairs: function(tbl){
        var _set_side = function(tbl, wrap, side_class){
            var chairs = wrap.find(side_class);

            if (side_class == '.chair.left-side' || side_class == '.chair.right-side' ){
                var division = tbl.height() / chairs.length;

                var division_percent = division * 100 / tbl.height();

                $.each(chairs, function(index, item){
                    if (chairs.length == 1){
                        index = 0.5;
                        division = division / 2;
                        division_percent = division_percent / 2;
                    }
                    var chair = $(item);
                    chair.css({
                        'top': index * division + 'px',
                        'height': division_percent + '%',
                    });
                });
            }
            if (side_class == '.chair.top-side' || side_class == '.chair.bottom-side' ){
                var division = tbl.width() / chairs.length;

                var division_percent = division * 100 / tbl.width();

                $.each(chairs, function(index, item){
                    if (chairs.length == 1){
                        index = 0.5;
                        division = division / 2;
                        division_percent = division_percent / 2;
                    }
                    var chair = $(item);
                    chair.css({
                        'left': index * division + 'px',
                        'width': division_percent + '%',
                    });
                });
            }
        }
        // compute chair heights and absolute positioning
        if (tbl.height() > tbl.width()){ // portrait
            tbl.find(".chair.odd").addClass("left-side");
            tbl.find(".chair.even").addClass("right-side");
        } else { // landscape
            tbl.find(".chair.odd").addClass("top-side");
            tbl.find(".chair.even").addClass("bottom-side");
        }
        var wrap = tbl.find(".chairsWrap");
        _set_side(tbl, wrap, ".chair.left-side");
        _set_side(tbl, wrap, ".chair.right-side");
        _set_side(tbl, wrap, ".chair.top-side");
        _set_side(tbl, wrap, ".chair.bottom-side");
    },
    tbl_opts_show: function(target_wrap){
        target_wrap.show();
        target_wrap.find("input[name='name']").val("").focus();
        // remove .drop-acceptable classes which might remain unremoved on hovering out the droppables
        $(".drop-acceptable").removeClass("drop-acceptable");
    },
    tbl_opts_hide: function(){
        // truncate the tbl opts modal and remove .tbl-opts-shown class from the dragged tbl
        // that class is added when modal is shown to restrict dropping on a droppable AND show the tbl opts
        $(".tbl-opts-wrap").hide();
        $(".tbl-opts-shown").removeClass("tbl-opts-shown");
    },
    tbl_opts_handle_exit: function(){
        var that = this;
        $("body").on("click", ".tbl-opts-wrap a.exit", function(event){
            that.tbl_opts_hide();
        });
    },
    handle_tbl: function(){
        this.handle_tbl_reserve();
        this.handle_tbl_free();
    },
    handle_tbl_reserve: function(){
        var that = this;
        $("body").on("submit", "form.reserve-table", function(event){
            event.preventDefault();
            var self = $(this);

            if (!self.hasClass("submitting")){
                self.addClass("submitting");
                $.ajax({
                    url: self.attr("action"),
                    method: self.attr("method"),
                    data: self.serialize(),
                    success: function(response){
                        self.removeClass("submitting");
                        // tbl specifics like prettify and drag_n_drop are handled on swampdragon channel
                    }
                });
            }
        });
    },
    handle_tbl_free: function(){
        var that = this;
        $("body").on("click", ".free-table", function(event){
            event.preventDefault();
            var self = $(this);
            if (!self.hasClass("submitting")){
                self.addClass("submitting");
                $.ajax({
                    url: self.attr("href"),
                    method: "POST",
                    data: {
                        csrfmiddlewaretoken: $("input[name='csrfmiddlewaretoken']").val(),
                    },
                    success: function(response){
                        self.removeClass("submitting");
                        // tbl specifics like prettify and drag_n_drop are handled on swampdragon channel
                    }
                });
            }
        });
    },
    handle_tbl_drag_n_drop: function(elements){
        // drag table to move customer to a different table
        var that = this;
        if (typeof(elements) == 'undefined'){
            elements = $(".tbl");
        }

        elements.draggable({
            addClasses: false,
            //containment: ".tablesWrap:visible",
            containment: ".sectorListWrap",
            revert: true,
            // cursor: "move",
            // dragstart
            start: function(event, ui){
                ui.helper.css("z-index", 100);
                ui.helper.closest(".sectorListWrap").addClass("tbl-dragging");
            },
            // dragstop
            stop: function(event, ui){
                ui.helper.css("z-index", 1);
                ui.helper.closest(".sectorListWrap").removeClass("tbl-dragging");
            },
            drag: function(event, ui){
            }
        });

        elements.droppable({
            addClasses: false,
            accept: function(draggable){
                var droppable = $(this);
                // draggable must have .red class - that is reserved tables
                // draggable must not have .tbl-opts-shown class - that is when tbl-opts are shown (see on drag above)
                // droppable must not have .red class - that is free tables to drop to
                // draggable must not be dragged on slideOpts - if so slideOpts div has class drop-acceptable
                if ($(".slideOpts div.drop-acceptable").length != 0){
                    return false;
                }
                if (draggable.hasClass("red") && !draggable.hasClass("tbl-opts-shown") && !droppable.hasClass("red")){
                    return true;
                }
                return false;
            },
            hoverClass: "drop-acceptable",  // droppable gets this class when acceptable draggable hovers it
            tolerance: "touch",
            over: function(event, ui){
            },
            out: function(event, ui){
            },
            drop: function(event, ui){
                var dragged = ui.helper;
                var dropped = $(this);

                // emulate dropping functionality just for one tbl by setting the dropping flag class
                // which will get removed on ajax success in free dragged table
                if (!dragged.hasClass("dropping")){
                    dragged.addClass("dropping");

                    // build modal to confirm the drop
                    var confirm_modal_id = "confirmDrop";
                    var selector = "#" + confirm_modal_id;
                    $(selector).remove();  // remove previously created

                    var dragged_tbl_opts = dragged.next(".tbl-opts-wrap");
                    var switch_helper = dragged_tbl_opts.find(".table-switch-helper");
                    var client_name = switch_helper.data("name");

                    var header = $("<h5/>").addClass("text-center").text("Te rugam sa confirmi");
                    var body = $("<p/>").addClass("text-center").html("Ai ales sa muti clientul <strong>" + client_name + "</strong> de la masa <strong>" + dragged.data("number") + "</strong> la masa <strong>" + dropped.data("number") + "</strong>");
                    var submit = $("<a/>").addClass("confirm-drop btn btn-success pull-left").attr({
                        "data-dragged": "#" + dragged.attr("id"),
                        "data-dropped": "#" + dropped.attr("id"),
                    }).text("Muta");
                    
                    modal = that.create_modal(confirm_modal_id, header, body, submit);
                    $("body").append(modal);
                    $(selector).modal("show");
                }
            }
        });

        $(".slideOpts").each(function(index, item){
            var item = $(item);
            if (!item.hasClass("has-droppables")){
                item.addClass("has-droppables");
                item.find("div.left, div.right").droppable({
                    addClasses: false,
                    hoverClass: "drop-acceptable",
                    tolerance: "touch",
                    accept: function(draggable){
                        var droppable = $(this);
                        return true;
                    },
                    over: function(event, ui){
                        // clear all drop targets in case the class remained on a droppable
                        $(".tbl.drop-acceptable").removeClass("drop-acceptable");
                    },
                    drop: function(event, ui){
                        var dragged = ui.helper;
                        var dropped = $(this);
                        if (dropped.hasClass("left")){
                            dragged.addClass('tbl-opts-shown');  // droppable accept function rejects draggables with this class
                            var element = dragged.next(".tbl-opts-wrap");
                            that.tbl_opts_show(element);
                        }
                        if (dropped.hasClass("right")){
                            dragged.addClass('tbl-opts-shown');  // droppable accept function rejects draggables with this class
                            var element = dragged.next(".tbl-opts-wrap");
                            that.tbl_opts_show(element);
                        }
                    }
                });
            }
        });

    },
    handle_confirm_drop: function(){
        $("body").on("click", "#confirmDrop a.confirm-drop", function(event){
            event.preventDefault();
            var self = $(this);
            var dragged = $(self.data("dragged"));
            var dropped = $(self.data("dropped"));

            // free dragged table
            var dragged_tbl_opts = dragged.next(".tbl-opts-wrap");
            dragged_tbl_opts.find("a.free-table").click();

            var switch_helper = dragged_tbl_opts.find(".table-switch-helper");

            // reserve dropped table
            var form = dropped.next(".tbl-opts-wrap").find("form");
            form.find("input.name").val(switch_helper.data("name"));
            form.find("input.phone").val(switch_helper.data("phone"));
            form.find("input.email").val(switch_helper.data("email"));
            setTimeout(function(){
                form.submit();
            }, 500);

            self.parent().find("button[data-dismiss='modal']").click();

            // display message
            var message = "Ai mutat clientul de la masa " + dragged.data("number") + " la masa " + dropped.data("number");
            Utils.send_alert(message, 'success');

            if (dragged.data("chairs") > dropped.data("chairs")){
                var message = "Masa " + dropped.data("number") + " are mai putine scaune decat masa " + dragged.data("number");
                Utils.send_alert(message, 'warning');
            }
        });

        $("body").on("click", "#confirmDrop button[data-dismiss='modal']", function(event){
            var self = $(this);
            // just remove class dropping from the dragged tbl
            var dragged_selector = self.parent().find("a.confirm-drop").data("dragged");
            $(dragged_selector).removeClass("dropping");
        });
    },
    create_modal: function(id, header, body, submit){
        if (typeof(submit) == 'undefined'){
            submit = $("<button/>").addClass("btn btn-success pull-left").text("OK").attr("type", "submit");
        }
        var modal = $("<div/>").addClass("modal").attr({
            id: id,
            tabindex: -1,
            role: "dialog",
            "aria-labelledby": id + "Label"
        }).append(
            $("<div/>").addClass("modal-dialog").attr("role", "document").append(
                $("<div/>").addClass("modal-content").append(
                    $("<div/>").addClass("modal-header").html(header),
                    $("<div/>").addClass("modal-body").html(body),
                    $("<div/>").addClass("modal-footer").append(
                        submit,
                        $("<button/>").addClass("btn btn-danger").text("Anuleaza").attr("data-dismiss", "modal")
                    )
                )
            )
        )
        return modal;
    },
    get_sector: function(id){
        return $(".sector[data-id='" + id + "']");
    },
    get_table: function(id){
        return $(".tbl[data-id='" + id + "']");
    },
    call_the_dragons: function(){
        var that = this;
        swampdragon.onChannelMessage(function (channels, message) {
            var data = message.data;

            if (message.action == 'created'){
                swampdragon.callRouter('get_one_tbl_html', 'table_router', {'pk': data.id}, function(context, data){
                    var tblWrap = $("<div/>").addClass("tblWrap").append($(data.html))
                    $(".tablesWrap:visible").append(tblWrap);

                    var tbl = tblWrap.find(".tbl");
                    that.prettify_tbl(tbl);
                    that.handle_tbl_drag_n_drop(tbl);
                });
            }
            if (message.action == 'updated'){
                var table_in_dom = that.get_table(data.id);
                var container = table_in_dom.closest(".tblWrap");
                swampdragon.callRouter('get_one_tbl_html', 'table_router', {'pk': data.id}, function(context, data){
                    container.html(data.html);

                    var tbl = container.find(".tbl");
                    that.prettify_tbl(tbl);
                    that.handle_tbl_drag_n_drop(tbl);
                });
            }
            if (message.action == 'deleted'){
                var tbl = that.get_table(data.id);
                tbl.closest(".tblWrap").remove();
            }
            // update sector free tables counter
            var sector = $(".sectorWrap .sector-free-tables")
            swampdragon.callRouter('get_free_tables', 'sector_router', {'pk': sector.data("id")}, function(context, data){
                sector.text(data.free_tables);
            });
        });
    },
    handle_hashed_scroll: function(){
        if (location.hash){
            Utils.scroll_animate($(location.hash), -80, 500);
        }
    },
}
$(function(){
    SectorDetail.init();

    $(window).on("resize", function(){
        SectorDetail.scale_sector_canvas();
    });
});

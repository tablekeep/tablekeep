# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import swampdragon.models
import django.db.models.deletion
from django.conf import settings
import tablekeep.apps.locations.models


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Booking',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=128)),
                ('phone', models.CharField(max_length=16)),
                ('email', models.EmailField(max_length=64, null=True, blank=True)),
                ('person_count', models.PositiveIntegerField()),
                ('timestamp', models.DateTimeField()),
                ('created', models.DateTimeField(auto_now_add=True)),
            ],
            options={
                'ordering': ['-created'],
            },
        ),
        migrations.CreateModel(
            name='Client',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=128)),
                ('phone', models.CharField(default=b'', max_length=16, blank=True)),
                ('email', models.EmailField(max_length=64, null=True, blank=True)),
                ('created', models.DateTimeField(auto_now_add=True)),
            ],
            options={
                'ordering': ['-created'],
            },
        ),
        migrations.CreateModel(
            name='Employee',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('role', models.IntegerField(default=1, choices=[(1, b'Hostess'), (2, b'Chelner'), (3, b'Sef sala'), (4, b'CEO')])),
                ('can_configure_location', models.BooleanField(default=False)),
            ],
        ),
        migrations.CreateModel(
            name='Location',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=512)),
                ('address', models.CharField(max_length=512)),
                ('logo', models.ImageField(null=True, upload_to=tablekeep.apps.locations.models.location_upload_to, blank=True)),
                ('rank', models.IntegerField(default=0, help_text=b'Special ordering')),
                ('created', models.DateTimeField(auto_now_add=True)),
            ],
            options={
                'ordering': ['-created'],
            },
        ),
        migrations.CreateModel(
            name='Sector',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('number', models.PositiveIntegerField()),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('created_by', models.ForeignKey(to='locations.Employee')),
                ('location', models.ForeignKey(to='locations.Location')),
            ],
            options={
                'ordering': ['number'],
            },
            bases=(swampdragon.models.SelfPublishModel, models.Model),
        ),
        migrations.CreateModel(
            name='Table',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('number', models.PositiveIntegerField()),
                ('chair_count', models.PositiveIntegerField()),
                ('layout', models.CharField(default=b'', help_text=b'The attr style of the table - if set with jquery ui', max_length=256, blank=True)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('created_by', models.ForeignKey(to='locations.Employee')),
            ],
            options={
                'ordering': ['number'],
            },
            bases=(swampdragon.models.SelfPublishModel, models.Model),
        ),
        migrations.CreateModel(
            name='TableClient',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('start', models.DateTimeField()),
                ('end', models.DateTimeField(null=True, blank=True)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('client', models.ForeignKey(to='locations.Client')),
                ('created_by', models.ForeignKey(to='locations.Employee')),
                ('table', models.ForeignKey(to='locations.Table')),
            ],
        ),
        migrations.AddField(
            model_name='table',
            name='reservation',
            field=models.ForeignKey(related_name='reservation', on_delete=django.db.models.deletion.SET_NULL, blank=True, to='locations.TableClient', null=True),
        ),
        migrations.AddField(
            model_name='table',
            name='sector',
            field=models.ForeignKey(to='locations.Sector'),
        ),
        migrations.AddField(
            model_name='employee',
            name='location',
            field=models.ForeignKey(to='locations.Location'),
        ),
        migrations.AddField(
            model_name='employee',
            name='user',
            field=models.OneToOneField(to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='client',
            name='created_by',
            field=models.ForeignKey(to='locations.Employee'),
        ),
        migrations.AddField(
            model_name='booking',
            name='created_by',
            field=models.ForeignKey(to='locations.Employee'),
        ),
        migrations.AddField(
            model_name='booking',
            name='location',
            field=models.ForeignKey(to='locations.Location'),
        ),
        migrations.AddField(
            model_name='booking',
            name='table',
            field=models.ForeignKey(to='locations.Table'),
        ),
        migrations.AlterUniqueTogether(
            name='table',
            unique_together=set([('sector', 'number')]),
        ),
        migrations.AlterUniqueTogether(
            name='sector',
            unique_together=set([('location', 'number')]),
        ),
    ]

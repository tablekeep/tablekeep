from django import forms
from django.conf import settings

from collections import OrderedDict

from tablekeep.apps.locations.models import Employee, Location, Sector, Table, Booking
from tablekeep.apps.base.forms import FunkyForm


class LocationForm(FunkyForm, forms.ModelForm):
    created_by = forms.ModelChoiceField(
        queryset=Employee.objects.all(), widget=forms.HiddenInput())
    modified_by = forms.ModelChoiceField(
        queryset=Employee.objects.all(), widget=forms.HiddenInput(),
        required=False)

    class Meta:
        fields = '__all__'
        model = Location


class SectorForm(FunkyForm, forms.ModelForm):
    location = forms.ModelChoiceField(
        queryset=Location.objects.all(), widget=forms.HiddenInput())
    created_by = forms.ModelChoiceField(
        queryset=Employee.objects.all(), widget=forms.HiddenInput())
    modified_by = forms.ModelChoiceField(
        queryset=Employee.objects.all(), widget=forms.HiddenInput(),
        required=False)

    class Meta:
        fields = '__all__'
        model = Sector


class TableForm(FunkyForm, forms.ModelForm):
    sector = forms.ModelChoiceField(
        queryset=Sector.objects.all(), widget=forms.HiddenInput())
    created_by = forms.ModelChoiceField(
        queryset=Employee.objects.all(), widget=forms.HiddenInput())
    number = forms.IntegerField(widget=forms.HiddenInput())
    chair_count = forms.IntegerField(label='Numar scaune', min_value=1, max_value=10)

    class Meta:
        exclude = ('reservation', 'layout')
        model = Table


class BookingForm(FunkyForm, forms.ModelForm):
    created_by = forms.ModelChoiceField(
        queryset=Employee.objects.all(), widget=forms.HiddenInput())
    location = forms.ModelChoiceField(
        queryset=Location.objects.all(), widget=forms.HiddenInput())
    table = forms.ModelChoiceField(
        queryset=Table.objects.all(), widget=forms.HiddenInput())
    timestamp = forms.DateTimeField(input_formats=settings.DATETIME_INPUT_FORMAT)

    class Meta:
        fields = '__all__'
        model = Booking

    def __init__(self, *args, **kwargs):
        super(BookingForm, self).__init__(*args, **kwargs)

        label_map = {
            'person_count': 'Numar persoane',
            'phone': 'Telefon',
            'name': 'Nume',
            'timestamp': 'Ora si data rezervarii',
        }

        for name, field in self.fields.items():
            label = label_map.get(name, None)
            if label is not None:
                field.label = label

            if field.required:
                field.widget.attrs.update({'required': 'required'}) 

            field.widget.attrs.update({'autocomplete': 'off'})


BookingForm.base_fields = OrderedDict(                                            
    (k, BookingForm.base_fields[k])                                         
    for k in ['timestamp', 'person_count', 'name', 'phone', 'email', 'table', 'created_by', 'location']
) 


from django import forms
from django.contrib.auth.forms import (
    UserCreationForm, AuthenticationForm, UserChangeForm
)
from django.contrib.auth.models import User
# MOCKUP 


class UserCreateForm(UserCreationForm):
    class Meta:
        model = User
        fields = ('username', 'email', )


class EmployeeCreateForm(forms.ModelForm):
    class Meta:
        model = Employee
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        self.uf = UserCreateForm(*args, **kwargs)

        super(EmployeeCreateForm, self).__init__(*args, **kwargs)

        self.fields.update(self.uf.fields)
        self.initial.update(self.uf.initial)

        self.fields['rank'].widget = forms.HiddenInput()
        self.fields['user'].widget = forms.HiddenInput()
        self.fields['user'].required = False  # set in save below
        self.fields['email'].required = True

        # set labels
        for field, label in self.label_map.items():
            self.fields.get(field).label = label

        # add gumby classes to prettify form
        for name, field in self.fields.items():
            if not hasattr(field, 'choices'):
                field.widget.attrs.update({'class': 'input'})
            if name in ['services', 'resume']:
                field.widget.attrs.update({
                    'class': 'input textarea',
                    'rows': 5
                })
            field.widget.attrs.update({'placeholder': field.label})
            # mark required fields
            if field.required:
                field.widget.attrs.update({'data-required': 'required'})

    @property
    def label_map(self):
        return {
            'username': 'Utilizator',
            'display_name': 'Nume profil',
        }

    def is_valid(self):
        """
        Updates the errors with suer form errors so both forms get validated.
        """
        self.errors.update(self.uf.errors)
        return super(EmployeeCreateForm, self).is_valid()

    def save(self, *args, **kwargs):
        """
        Saves the user and attaches it to the employee. Returns the employee
        """
        user = self.uf.save(*args, **kwargs)
        employee = super(EmployeeCreateForm, self).save(*args, commit=False, **kwargs)
        employee.user = user
        employee.save()
        return employee



import datetime
import json

from collections import defaultdict

from django.core.urlresolvers import reverse
from django.conf import settings
from django.http import HttpResponse, Http404, HttpResponseRedirect
from django.shortcuts import get_object_or_404
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt

from django.views.generic import (
    View, TemplateView, ListView, DetailView, CreateView, UpdateView,
    DeleteView
)

from tablekeep.apps.base.views import LoginRequiredMixin
from tablekeep.apps.base.log import logger

from .models import Location, Sector, Table, TableClient, Client, Booking
from .forms import LocationForm, SectorForm, TableForm, BookingForm


class ArchitectMixin(object):
    """
    Requires the can_configure_location flag to be set to True
    """
    def dispatch(self, request, *args, **kwargs):
        if not request.user.employee.can_configure_location:
            raise Http404('Not allowed to configure location or layouts')

        return super(ArchitectMixin, self).dispatch(request, *args, **kwargs)


class EmployeeIndexView(TemplateView):
    template_name = 'locations/index.html'


class LocationConfigView(LoginRequiredMixin, ArchitectMixin, TemplateView):
    template_name = 'locations/location_config.html'
    http_method_names = [u'get', u'post']

    def get_context_data(self, **kwargs):
        context = super(LocationConfigView, self).get_context_data(**kwargs)
        context.update({
            'location': self.location
        })
        return context

    def get(self, request, *args, **kwargs):
        self.location = get_object_or_404(Location, id=self.kwargs.get('pk'))

        # NOTE: once configuration is done redirect to layouts here
        # this is temporary see TODO in self.post
        if self.location.sector_set.count():
            return HttpResponseRedirect(self.location.get_layout_url())

        return super(LocationConfigView, self).get(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = self.process_location_data()

        self.location = get_object_or_404(Location, id=self.kwargs.get('pk'))

        # TODO: dont start with a clean slate coz we wipe out all sector layouts

        # start with a clean slate
        self.location.sector_set.all().delete()

        # manually create sectors and tables for this location
        for sector_number, sector_tables in data.items():
            sector = Sector.objects.create(
                location=self.location,
                number=sector_number,
                created_by=self.request.user.employee,
            )
            for table_data in sector_tables:
                table = Table.objects.create(
                    sector=sector,
                    created_by=self.request.user.employee,
                    **table_data
                )
        redirect_url = reverse('location_layout', args=[self.location.id])

        return HttpResponse(redirect_url)

    def process_location_data(self):
        location_data = self.request.POST.get('location_data')

        data = json.loads(location_data)

        processed_data = defaultdict(list)

        for item in data:
            processed_data[item.get('sector')].append({
                'chair_count': item.get('chair_count'),
                'number': item.get('table')
            })

        return processed_data


class LocationLayoutView(LoginRequiredMixin, ArchitectMixin, TemplateView):
    template_name = 'locations/location_layout.html'
    http_method_names = [u'get']

    def get_context_data(self, **kwargs):
        context = super(LocationLayoutView, self).get_context_data(**kwargs)
        context.update({
            'location': self.location
        })
        return context

    def get(self, request, *args, **kwargs):
        self.location = get_object_or_404(Location, id=self.kwargs.get('pk'))
        return super(LocationLayoutView, self).get(request, *args, **kwargs)


class LocationDetailView(LoginRequiredMixin, DetailView):
    model = Location
    template_name = 'locations/location_detail.html'

    def get_context_data(self, *args, **kwargs):
        context = super(LocationDetailView, self).get_context_data(*args, **kwargs)

        context.update({
        })
        return context


class SectorLayoutView(LoginRequiredMixin, ArchitectMixin, TemplateView):
    template_name = 'locations/sector_layout.html'
    http_method_names = [u'get', u'post']

    def get_context_data(self, **kwargs):
        context = super(SectorLayoutView, self).get_context_data(**kwargs)
        context.update({
            'sector': self.sector
        })
        return context

    def get(self, request, *args, **kwargs):
        self.sector = get_object_or_404(Sector, id=self.kwargs.get('pk'))
        return super(SectorLayoutView, self).get(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        self.sector = get_object_or_404(Sector, id=self.kwargs.get('pk'))

        layout = request.POST.get('layout', None)
        if layout is None:
            raise Http404

        try:
            layout = json.loads(layout)
        except Exception, exc:
            logger.error(exc)
            raise Http404

        for item in layout:
            table = self.sector.table_set.get(id=item.get('id'))
            table.layout = item.get('style')
            table.save()

        return HttpResponse(self.sector.location.get_layout_url())  # redirect url in ajax success


class SectorCreateView(LoginRequiredMixin, ArchitectMixin, CreateView):
    model = Sector
    form_class = SectorForm
    template_name = 'locations/sector_form.html'
    http_method_names = [u'post', u'get']

    def get_initial(self):
        initial = {
            'location': self.kwargs.get('pk'),
            'created_by': self.request.user.employee,
            'number': self.get_sector_next_number(),
        }
        return initial

    def get_sector_next_number(self):
        location = get_object_or_404(Location, id=self.kwargs.get('pk'))
        numbers = location.sector_set.order_by('number').values_list('number', flat=True)

        if not numbers:
            return 1

        # if a gap is found break out and return the number
        for number in range(1, max(numbers) + 1):
            if number not in numbers:
                break

        # if no gap is found, ie number is the biggest among numbers increase it
        if number == max(numbers):
            number += 1

        return number


    def get_context_data(self, **kw):
        context = super(SectorCreateView, self).get_context_data(**kw)

        action = reverse('sector_create', args=[self.kwargs.get('pk')])
        context.update({
            'action': action
        })
        return context

    def form_valid(self, form):
        self.object = form.save()
        self.template_name = 'locations/include/one_sector_layout.html'
        return self.render_to_response({'sector': self.object})

    def form_invalid(self, form):
        return HttpResponse(json.dumps(form.errors),
                            content_type='application/json')


class SectorListView(LoginRequiredMixin, ListView):
    model = Sector
    template_name = 'locations/sector_list.html'

    def get_queryset(self, *args, **kwargs):
        location_id = self.kwargs.get('location_id')

        return self.model.objects.filter(
            location__id=location_id
        ).prefetch_related('table_set')


class TableChairView(LoginRequiredMixin, ArchitectMixin, View):
    http_method_names = [u'post']
    operator = None  # coming from urls - plus or minus

    def post(self, request, *args, **kwargs):
        table = get_object_or_404(Table, id=self.kwargs.get('pk'))

        if self.operator is None:
            raise Http404

        if self.operator == 'plus':
            # max 10 chairs for the moment
            if table.chair_count == 10:
                raise Http404

            table.chair_count += 1

        if self.operator == 'minus':
            # a table needs at least one chair, right?
            if table.chair_count == 1:
                raise Http404

            table.chair_count -= 1

        table.save()

        return HttpResponse(table.chair_count)


class TableDeleteView(LoginRequiredMixin, ArchitectMixin, DeleteView):
    model = Table
    http_method_names = [u'post']

    def delete(self, request, *args, **kwargs):
        self.object = self.get_object()
        self.object.delete()
        return HttpResponse('OK')


class TableCreateView(LoginRequiredMixin, ArchitectMixin, CreateView):
    model = Table
    form_class = TableForm
    template_name = 'locations/table_form.html'
    http_method_names = [u'post', u'get']

    def get_success_url(self):
        return self.object.get_absolute_url()

    def get_initial(self):
        initial = {
            'sector': self.kwargs.get('pk'),
            'number': self.get_table_next_number(),
            'created_by': self.request.user.employee,
        }
        return initial

    def get_table_next_number(self):
        sector = get_object_or_404(Sector, id=self.kwargs.get('pk'))
        numbers = sector.table_set.order_by('number').values_list('number', flat=True)

        if not numbers:
            return 1

        # if a gap is found break out and return the number
        for number in range(1, max(numbers) + 1):
            if number not in numbers:
                break

        # if no gap is found, ie number is the biggest among numbers increase it
        if number == max(numbers):
            number += 1

        return number

    def get_context_data(self, **kw):
        context = super(TableCreateView, self).get_context_data(**kw)

        action = reverse('table_create', args=[self.kwargs.get('pk')])

        context.update({
            'action': action
        })
        return context

    def form_valid(self, form):
        self.object = form.save()

        self.template_name = 'locations/include/one_tbl.html'
        return self.render_to_response({'table': self.object, 'config_mode': True})

    def form_invalid(self, form):
        return HttpResponse(json.dumps(form.errors),
                            content_type='application/json')


class TableReserveView(LoginRequiredMixin, TemplateView):
    http_method_names = [u'post']

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(TableReserveView, self).dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        table_id = kwargs.get('table_id')
        table = Table.objects.get(id=table_id)

        name = request.POST.get('name')
        phone = request.POST.get('phone')
        email = request.POST.get('email')

        # create the client
        client = Client.objects.create(name=name, phone=phone, email=email,
                                       created_by=request.user.employee)
        # create the table client
        reservation = TableClient.objects.create(
            table_id=table_id, client=client, start=datetime.datetime.now(),
            created_by=request.user.employee,
        )
        # set the reservation on the table
        table.reservation = reservation
        table.save()

        self.template_name = 'locations/include/one_tbl.html'
        return self.render_to_response({'table': table})


class TableFreeView(LoginRequiredMixin, TemplateView):
    http_method_names = [u'post']

    def post(self, request, *args, **kwargs):
        table_id = kwargs.get('table_id')
        table = Table.objects.get(id=table_id)

        # set the reservation end time on the table
        table.reservation.end = datetime.datetime.now()
        table.reservation.save()

        table.reservation = None
        table.save()

        self.template_name = 'locations/include/one_tbl.html'
        return self.render_to_response({'table': table})


class TableClientListView(LoginRequiredMixin, ListView):
    model = TableClient
    template_name = 'locations/tableclient_list.html'

    def get_queryset(self, *args, **kw):
        qs = super(TableClientListView, self).get_queryset(*args, **kw)

        location_id = self.kwargs.get('location_id')

        qs = qs.filter(table__sector__location_id=location_id)

        return qs.filter(end__isnull=True).order_by('start')


class BookingCreateView(LoginRequiredMixin, CreateView):
    model = Booking
    form_class = BookingForm
    template_name = 'locations/booking_form.html'
    http_method_names = [u'post', u'get']

    def get_initial(self):
        initial = {
            'location': self.kwargs.get('location_id'),
            'created_by': self.request.user.employee.id,
        }
        return initial

    def get_context_data(self, **kw):
        context = super(BookingCreateView, self).get_context_data(**kw)
        context.update({
            'location_id': self.kwargs.get('location_id'),
        })
        return context

    def get_success_url(self):
        return reverse('booking_list', args=[self.kwargs.get('location_id')])


class BookingSlotView(LoginRequiredMixin, TemplateView):
    http_method_names = [u'get']
    template_name = 'locations/include/booking_slot.html'

    def get_context_data(self, **kw):
        context = super(BookingSlotView, self).get_context_data(**kw)
        tables = self.fetch_tables()
        interval_bookings = self.fetch_interval_bookings(tables)

        context.update({
            'tables': tables,
            'intervals': self.fetch_booking_intervals(),
            'interval_bookings': interval_bookings,
            'timestamp': self.get_timestamp(),
            'chair_count': int(self.request.GET.get('chair_count')),
        })
        return context

    def fetch_tables(self):
        location = get_object_or_404(
            Location.objects.prefetch_related('sector_set'),
            id=self.kwargs.get('location_id')
        )

        chair_count = self.request.GET.get('chair_count', None)
        if chair_count is None:
            raise Http404('No chair count')

        tables = Table.objects.filter(
            sector__location=location, chair_count__gte=chair_count
        ).order_by('chair_count', 'number', 'sector__number')
        return tables

    def get_timestamp(self):
        # eg: 20:30 01-Dec-2015
        timestamp_string = self.request.GET.get('timestamp', None)
        if timestamp_string is None:
            raise Http404('No timestmap')

        timestamp = datetime.datetime.strptime(
            timestamp_string, '%H:%M %d-%b-%Y'
        )
        return timestamp

    def fetch_booking_intervals(self):
        timestamp = self.get_timestamp()
        gap = settings.LOCATIONS_BOOKING_GAP

        # get x hours before and after
        x = 2
        start = timestamp - datetime.timedelta(0, 60 * 60 * x)
        end = timestamp + datetime.timedelta(0, 60 * 60 * x)
        delta_gap = datetime.timedelta(0, gap)

        intervals = []
        while start <= end:
            intervals.append((start, start + delta_gap))
            start += delta_gap

        return intervals
    
    def fetch_interval_bookings(self, tables):
        interval_bookings = defaultdict(dict)
        intervals = self.fetch_booking_intervals()

        timestamp = self.get_timestamp()

        start = timestamp.replace(hour=0, minute=0)
        end = timestamp.replace(hour=23, minute=59)

        qs = Booking.objects.filter(
            timestamp__range=[start, end],
            location_id=self.kwargs.get('location_id')
        )

        for interval in intervals:
            start, end = interval
            for table in tables:
                interval_bookings[interval][table] = qs.filter(
                    timestamp__gte=start,
                    timestamp__lt=end,
                    table=table
                )
        return interval_bookings


class BookingListView(LoginRequiredMixin, ListView):
    model = Booking
    template_name = 'locations/booking_list.html'

    def get_queryset(self, *args, **kw):
        qs = super(BookingListView, self).get_queryset(*args, **kw)

        location_id = self.kwargs.get('location_id')

        # filter out bookings starting from today for this location
        today = datetime.datetime.today().replace(hour=0, minute=0, second=0)
        qs = qs.filter(location_id=location_id, timestamp__gte=today)

        return qs.order_by('timestamp')



# Below views are not used
class BaseCreateView(CreateView):
    http_method_names = [u'post', u'get']

    def get_success_url(self):
        return self.object.get_absolute_url()

    def get_initial(self):
        return {
            'created_by': self.request.user.employee,
        }


class BaseUpdateView(UpdateView):
    http_method_names = [u'post', u'get']

    def get_success_url(self):
        return self.object.get_absolute_url()

    def get_initial(self):
        return {
            'modified_by': self.request.user.employee,
        }


class LocationCreateView(LoginRequiredMixin, BaseCreateView):
    model = Location
    form_class = LocationForm
    template_name = 'locations/location_form.html'


class LocationUpdateView(LoginRequiredMixin, BaseUpdateView):
    model = Location
    form_class = LocationForm
    template_name = 'locations/location_form.html'


class LocationListView(LoginRequiredMixin, ListView):
    model = Location
    template_name = 'locations/location_list.html'

    def get_queryset(self, *args, **kwargs):
        return self.model.objects.filter(
        )


class SectorUpdateView(LoginRequiredMixin, BaseUpdateView):
    model = Sector
    form_class = SectorForm
    template_name = 'locations/sector_form.html'


class SectorDetailView(LoginRequiredMixin, DetailView):
    model = Sector
    template_name = 'locations/sector_detail.html'


class TableUpdateView(LoginRequiredMixin, BaseUpdateView):
    model = Table
    form_class = TableForm
    template_name = 'locations/table_form.html'


class TableDetailView(LoginRequiredMixin, DetailView):
    model = Table
    template_name = 'locations/table_detail.html'


class TableListView(LoginRequiredMixin, ListView):
    model = Table
    template_name = 'locations/table_list.html'

    def get_queryset(self, *args, **kwargs):
        return self.model.objects.filter(
        )

from swampdragon.serializers.model_serializer import ModelSerializer


class SectorSerializer(ModelSerializer):
    class Meta:
        model = 'locations.Sector'


class TableSerializer(ModelSerializer):
    class Meta:
        model = 'locations.Table'
        # publish_fields = ('number', 'chair_count', 'reservation_id')

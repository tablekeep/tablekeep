from django.conf.urls import url
from . import views

urlpatterns = [
    # employee index
    url(r'^menu/$', views.EmployeeIndexView.as_view(),
        name='employee_index'),

    # location
    url(r'^(?P<pk>\d+)/configurare/$', views.LocationConfigView.as_view(),
        name='location_config'),
    url(r'^(?P<pk>\d+)/layout/$', views.LocationLayoutView.as_view(),
        name='location_layout'),
    url(r'^(?P<pk>\d+)/$', views.LocationDetailView.as_view(),
        name='location_detail'),


    url(r'^$', views.LocationListView.as_view(),
        name='location_list'),
    url(r'^adauga/$', views.LocationCreateView.as_view(),
        name='location_create'),
    url(r'^editeaza/(?P<pk>\d+)/$', views.LocationUpdateView.as_view(),
        name='location_update'),

    # sector
    # no need for location_id in below urls since we have the pk of a sector
    url(r'^sector/(?P<pk>\d+)/layout/$', views.SectorLayoutView.as_view(),
        name='sector_layout'),
    url(r'^sector/(?P<pk>\d+)/$', views.SectorDetailView.as_view(),
        name='sector_detail'),
    url(r'^(?P<pk>\d+)/sector/create/$', views.SectorCreateView.as_view(),
        name='sector_create'),

    # all sectors for a specific location
    url(r'^(?P<location_id>\d+)/sector/list/$', views.SectorListView.as_view(),
        name='sector_list'),

    # table
    url(r'^table/(?P<pk>\d+)/chair/plus/$',
        views.TableChairView.as_view(operator='plus'),
        name='table_chair_plus'),

    url(r'^table/(?P<pk>\d+)/chair/minus/$',
        views.TableChairView.as_view(operator='minus'),
        name='table_chair_minus'),

    url(r'^sector/(?P<pk>\d+)/table/create/$', views.TableCreateView.as_view(),
        name='table_create'),

    url(r'^table/(?P<pk>\d+)/delete/$',
        views.TableDeleteView.as_view(), name='table_delete'),

    url(r'^table/(?P<table_id>\d+)/reserve/$',  # reservation
        views.TableReserveView.as_view(), name='table_reserve'),

    url(r'^table/(?P<table_id>\d+)/free/$',  # unreservation
        views.TableFreeView.as_view(), name='table_free'),

    # tableclient
    url(r'^(?P<location_id>\d+)/tableclient/list/$',
        views.TableClientListView.as_view(), name='tableclient_list'),

    # booking
    url(r'^(?P<location_id>\d+)/booking/list/$',
        views.BookingListView.as_view(), name='booking_list'),
    url(r'^(?P<location_id>\d+)/booking/create/$',
        views.BookingCreateView.as_view(), name='booking_create'),
    url(r'^(?P<location_id>\d+)/booking/slot/$',
        views.BookingSlotView.as_view(), name='booking_slot'),
]

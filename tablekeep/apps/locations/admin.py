from django.contrib import admin

from .models import Location, Sector, Table, TableClient, Client, Booking


admin.site.register(Location)
admin.site.register(Sector)


class TableAdmin(admin.ModelAdmin):
    list_display = ['number', 'sector', 'is_reserved', 'chair_count']


admin.site.register(Table, TableAdmin)
admin.site.register(TableClient)


# Employee
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import User

from .models import Employee


class EmployeeAdminInline(admin.StackedInline):
    model = Employee
    can_delete = True
    verbose_name_plural = 'employee'


class UserAdminCustom(UserAdmin):
    inline = (EmployeeAdminInline, )


admin.site.unregister(User)
admin.site.register(User, UserAdminCustom)


class EmployeeAdmin(admin.ModelAdmin):
    list_display = ('user', 'role', 'location', 'can_configure_location')


admin.site.register(Employee, EmployeeAdmin)


class ClientAdmin(admin.ModelAdmin):
    list_display = ['name', 'phone', 'email']


admin.site.register(Client, ClientAdmin)


class BookingAdmin(admin.ModelAdmin):
    list_display = ('name', 'phone', 'email', 'person_count', 'timestamp', 'created', 'created_by', 'location', 'table')
    list_filter = ('location',)


admin.site.register(Booking, BookingAdmin)

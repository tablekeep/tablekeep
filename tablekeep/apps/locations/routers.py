from swampdragon import route_handler
from swampdragon.route_handler import ModelRouter

from .serializers import TableSerializer, SectorSerializer
from .models import Table, Sector


class TableRouter(ModelRouter):
    route_name = 'table_router'
    serializer_class = TableSerializer
    model = Table
    valid_verbs = ModelRouter.valid_verbs + [
        'get_one_tbl_html',
        'get_one_sector_tbl_html'
    ]

    def get_object(self, **kwargs):
        return self.model.objects.get(pk=kwargs['pk'])

    def get_query_set(self, **kwargs):
        return self.model.objects.all()

    def get_one_tbl_html(self, **kwargs):
        table = self.get_object(**kwargs)
        self.send({'html': table.get_one_tbl_html()})

    def get_one_sector_tbl_html(self, **kwargs):
        table = self.get_object(**kwargs)
        self.send({'html': table.get_one_sector_tbl_html()})


route_handler.register(TableRouter)


class SectorRouter(ModelRouter):
    route_name = 'sector_router'
    serializer_class = SectorSerializer
    model = Sector
    valid_verbs = ModelRouter.valid_verbs + [
        'get_free_tables'
    ]

    def get_object(self, **kwargs):
        return self.model.objects.get(pk=kwargs['pk'])

    def get_query_set(self, **kwargs):
        return self.model.objects.all()

    def get_free_tables(self, **kwargs):
        sector = self.get_object(**kwargs)
        self.send({'free_tables': sector.free_tables})


route_handler.register(SectorRouter)

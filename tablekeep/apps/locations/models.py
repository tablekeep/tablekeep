import datetime

from django.conf import settings
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
from django.db import models
from django.template.loader import render_to_string

from swampdragon.models import SelfPublishModel
from .serializers import TableSerializer, SectorSerializer


class Employee(models.Model):
    user = models.OneToOneField(User)
    ROLE = (
        (1, 'Hostess'),
        (2, 'Chelner'),
        (3, 'Sef sala'),
        (4, 'CEO'),
    )
    role = models.IntegerField(choices=ROLE, default=1)

    location = models.ForeignKey('Location')
    can_configure_location = models.BooleanField(default=False)

    def __unicode__(self):
        return self.name

    @property
    def name(self):
        return self.user.get_full_name() or self.user.username


def location_upload_to(instance, filename):
    return 'location/logo/{id}/{filename}'.format(
        id=instance.id,
        filename=filename
    )


class Location(models.Model):
    not_required = {'blank': True, 'null': True}

    name = models.CharField(max_length=512)
    address = models.CharField(max_length=512)
    logo = models.ImageField(upload_to=location_upload_to, **not_required)
    rank = models.IntegerField(default=0, help_text='Special ordering')

    created = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ['-created']

    def __unicode__(self):
        return self.name

    @property
    def logo_url(self):
        if not self.logo:
            return '{}css/images/image_placeholder_100.png'.format(
                settings.STATIC_URL
            )
        return self.logo.url

    def get_absolute_url(self):
        return reverse('location_detail', args=[self.id])

    def get_layout_url(self):
        return reverse('location_layout', args=[self.id])

    def get_config_url(self):
        return reverse('location_config', args=[self.id])


class Sector(SelfPublishModel, models.Model):
    serializer_class = SectorSerializer

    location = models.ForeignKey(Location)
    number = models.PositiveIntegerField()

    created = models.DateTimeField(auto_now_add=True)
    created_by = models.ForeignKey(Employee)

    class Meta:
        ordering = ['number']
        unique_together = ('location', 'number')

    def __unicode__(self):
        return u'%s - Sector %s' % (self.location, self.number)

    def get_absolute_url(self):
        return reverse('sector_detail', args=[self.location.id, self.id])

    @property
    def is_arranged(self):
        return self.table_set.exclude(layout='').count() != 0

    @property
    def free_tables(self):
        return self.table_set.filter(reservation__isnull=True).count()


class Table(SelfPublishModel, models.Model):
    serializer_class = TableSerializer

    sector = models.ForeignKey(Sector)
    number = models.PositiveIntegerField()

    chair_count = models.PositiveIntegerField()

    layout = models.CharField(
        max_length=256, default='', blank=True,
        help_text='The attr style of the table - if set with jquery ui')

    created = models.DateTimeField(auto_now_add=True)
    created_by = models.ForeignKey(Employee)

    reservation = models.ForeignKey(
        'TableClient', blank=True, null=True, related_name='reservation',
        on_delete=models.SET_NULL
    )

    class Meta:
        ordering = ['number']
        unique_together = ('sector', 'number')

    def __unicode__(self):
        return u'%s - masa %s' % (self.sector, self.number)

    def get_absolute_url(self):
        # add a hash tag to highlight it once in sector detail
        return '{base_url}#tbl-{hash_tag}'.format(
            base_url=reverse('sector_detail', args=[self.sector.id]),
            hash_tag=self.id
        )

    @property
    def is_reserved(self):
        return self.reservation is not None

    @property
    def chair_range(self):
        return range(self.chair_count)

    def get_one_tbl_html(self):
        return render_to_string('locations/include/one_tbl.html', {
            'table': self
        })

    def get_one_sector_tbl_html(self):
        return render_to_string('locations/include/one_sector_tbl.html', {
            'table': self
        })

    def upcoming_bookings(self, gap=settings.LOCATIONS_BOOKING_GAP):
        now = datetime.datetime.now()
        end = now + datetime.timedelta(0, gap)
        return self.booking_set.filter(
            location=self.sector.location,
            timestamp__range=[now, end]
        ).order_by('timestamp')


class TableClient(models.Model):
    table = models.ForeignKey(Table)
    client = models.ForeignKey('Client')

    start = models.DateTimeField()
    end = models.DateTimeField(blank=True, null=True)

    created = models.DateTimeField(auto_now_add=True)
    created_by = models.ForeignKey('Employee')

    def __unicode__(self):
        return u'%s - %s' % (self.table, self.client)


class Client(models.Model):
    name = models.CharField(max_length=128)
    phone = models.CharField(max_length=16, default='', blank=True)
    email = models.EmailField(max_length=64, null=True, blank=True)

    created = models.DateTimeField(auto_now_add=True)
    created_by = models.ForeignKey(Employee)

    class Meta:
        ordering = ['-created']

    def __unicode__(self):
        return self.name

    def save(self, *args, **kwargs):
        self.name = self.name.title()
        super(Client, self).save(*args, **kwargs)


class Booking(models.Model):
    name = models.CharField(max_length=128)
    phone = models.CharField(max_length=16)
    email = models.EmailField(max_length=64, null=True, blank=True)

    person_count = models.PositiveIntegerField()
    timestamp = models.DateTimeField()  # help_text='For what date and time')

    created = models.DateTimeField(auto_now_add=True)
    created_by = models.ForeignKey(Employee)

    location = models.ForeignKey(Location)
    table = models.ForeignKey(Table)

    class Meta:
        ordering = ['-created']

    def __unicode__(self):
        return self.name

    def save(self, *args, **kwargs):
        self.name = self.name.title()
        super(Booking, self).save(*args, **kwargs)

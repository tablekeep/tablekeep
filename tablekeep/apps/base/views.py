from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.auth.views import login
from django.core.urlresolvers import reverse, reverse_lazy
from django.http import Http404, HttpResponseRedirect, HttpResponse
from django.shortcuts import render
from django.utils.decorators import method_decorator
from django.views.generic import TemplateView, FormView

from .forms import UserForm, PasswordForm


class LoginRequiredMixin(object):
    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(LoginRequiredMixin, self).dispatch(request, *args, **kwargs)


class IndexView(TemplateView):
    template_name = 'base/index.html'

    def get(self, request, *args, **kwargs):
        if request.user.is_authenticated():
            #return HttpResponseRedirect(reverse('employee_index'))
            return HttpResponseRedirect(
                request.user.employee.location.get_absolute_url()
            )
        return HttpResponseRedirect(reverse('login'))


def user_login(request, template='base/login.html'):
    if request.user.is_authenticated():
        return HttpResponseRedirect(reverse('index'))
    else:
        return login(request, template_name=template, extra_context={'login_flag': True})


class SettingsView(LoginRequiredMixin, FormView):
    http_method_names = [u'post', u'get']
    template_name = 'base/settings.html'
    form_class = UserForm
    success_url = reverse_lazy('settings')

    def get_form_kwargs(self):
        kwargs = super(SettingsView, self).get_form_kwargs()
        kwargs['instance'] = self.request.user
        return kwargs

    def form_valid(self, form):
        messages.add_message(self.request, messages.SUCCESS, 'Actualizat!')
        self.object = form.save()
        return HttpResponseRedirect(self.get_success_url())


class PasswordChangeView(LoginRequiredMixin, FormView):
    http_method_names = [u'post', u'get']
    template_name = 'base/password_form.html'
    form_class = PasswordForm
    success_url = reverse_lazy('settings')

    def get_form_kwargs(self):
        kwargs = super(PasswordChangeView, self).get_form_kwargs()
        kwargs['user'] = self.request.user
        return kwargs

    def form_valid(self, form):
        messages.add_message( self.request, messages.SUCCESS, 'Ai actualizat parola')
        self.object = form.save()
        return HttpResponseRedirect(self.get_success_url())


    def get_context_data(self, *args, **kwargs):
        context = super(PasswordChangeView, self).get_context_data(*args, **kwargs)
        context.update({
            'settings_flag': True,
            'password_flag': True,
        })
        return context

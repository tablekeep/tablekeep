Utils = {
    init_tooltips: function(){
        $(".ttip").tooltip();
    },
    init_closers: function(){
        $("body").on("click", "a.closer", function(event){
            event.preventDefault();
            var self = $(this);
            var container = self.closest(self.attr("data-close"));
            container.hide();
            if (self.hasClass("remove")){
                container.remove();
            }
            if ( typeof(container.attr("data-remove-class-target")) != 'undefined'){
                $(container.attr("data-remove-class-target")).removeClass(
                    container.attr("data-remove-class")
                )
            }
        });
        // closenox
        $(document).mouseup(function(e){
            var containers = $(".closenox");
 
            // if the target of the click is not the container
            // and not a descendant of the container
            $.each(containers, function(index, container){
                var container = $(container);
                if (!container.is(e.target) && container.has(e.target).length === 0){
                    container.hide();
                    if ( typeof(container.attr("data-remove-class-target")) != 'undefined'){
                        $(container.attr("data-remove-class-target")).removeClass(
                            container.attr("data-remove-class")
                        )
                    }
                    if ( typeof(container.attr("data-remove")) != 'undefined'){
                        $(container.attr("data-remove")).remove();
                    }
                    if ( typeof(container.attr("data-show")) != 'undefined'){
                        $(container.attr("data-show")).show();
                    }
                    if ( typeof(container.attr("data-remove-self-class")) != 'undefined'){
                        $(container).removeClass($(container).attr("data-remove-self-class"));
                    }
                    if (container.hasClass("remove")){
                        container.remove();
                    }
                }
            });
        });
    },
    prettify_tbl_number: function(tbl){
        // absolute position the tbl number
        if (tbl.width() > tbl.height()){
            // if landscape
            tbl.find(".number").css({
                'font-size': (tbl.height() * 0.8) + 'px',
                'line-height': '120%',
            });
        } else {
            // if portrait
            tbl.find(".number").css({
                'font-size': (tbl.width() * 0.8) + 'px',
                'line-height': tbl.height() + 'px',
            });
        }
    },
    make_draggable: function(selector, containment){
        var element = $(selector);
        element.draggable({
            containment: containment,
            //grid: [10, 10],
            cursor: "move",
            stop: function(event, ui){
                // on stop dragging we need to transform position and size from pixels to percent values 
                // so we nicely display the tables no matter the size of viewport

                var percent_left = (ui.position.left / $(containment).width() * 100) + "%";
                var percent_top = (ui.position.top / $(containment).height() * 100) + "%";

                var percent_width = (parseFloat($(this).css("width")) / $(containment).width() * 100) + "%";
                var percent_height = (parseFloat($(this).css("height")) / $(containment).height() * 100) + "%";

                $(this).css({
                    "left": percent_left,
                    "top": percent_top,
                    "width": percent_width,
                    "height": percent_height,
                });
            }
        });
    },
    make_resizable: function(selector, containment){
        var element = $(selector);
        element.resizable({
            //containment: containment,
            grid: 50,
            minWidth:50,
            minHeight:50,
            //animate: true,
            stop: function(event, ui){
                // on stop resizing we need to transform just the size from pixels to percent values 
                // so we nicely display the tables no matter the size of viewport
                
                var percent_width = (ui.size.width / $(containment).width() * 100) + "%";
                var percent_height = (ui.size.height / $(containment).height() * 100) + "%";

                $(this).css({
                    "width": percent_width,
                    "height": percent_height
                });
            }
        });
    },
    scroll_animate: function(element, extra_offset, miliseconds){
        // scroll whole html to jquery element offset within miliseconds
        // send a negative extra offset in case of a fixed header or something
        $("html, body").animate({
            scrollTop: element.offset().top + extra_offset
        }, miliseconds);
    },
    create_datetime_picker: function(selector){
        // http://www.malot.fr/bootstrap-datetimepicker/
        $(selector).datetimepicker({
            language: 'ro',
            autoclose: true,
            weekStart: 1,
            todayHighlight: true,
            minuteStep: 15,
            format: 'hh:ii dd-M-yyyy',
            minDate: 0,
            startDate: new Date(),
            //todayBtn: true,
        });
    },
    handle_ajax_modals: function(callbacks){
        // callbacks is an array of functions to callback in ajax success
        var that = this;
        $(".pull-modal").click(function(event){
            event.preventDefault();
            var self = $(this);

            if (!self.hasClass("pulling")){
                self.addClass("pulling");
                $.ajax({
                    url: self.attr("href"),
                    success: function(response){

                        $("body").append(response);
                        $(self.attr("data-modal-id")).modal("show");
                        self.removeClass("pulling");

                        that.init_tooltips();

                        // handle form submission once its pulled and appended into the dom
                        that._handle_ajax_pulled_modal_form(self.attr("data-modal-id"));
                    }
                })
            }
        });
    },
    _handle_ajax_pulled_modal_form: function(modal_selector){
        var that = this;
        // submit the form
        $(modal_selector).find("form").submit(function(event){
            event.preventDefault();
            var self = $(this);

            if(!self.hasClass("submitting")){
                self.addClass("submitting");

                // where to dump the response
                var container = $(self.data("container"));

                self.find(".errorlist").html('');

                $.ajax({
                    url: self.attr("action"),
                    method: self.attr("method"),
                    data: self.serialize(),
                    success: function(response){
                        if (typeof(response) == 'string'){
                            if (self.data("prepend")){
                                container.prepend(response);
                            } else if (self.data("append")){
                                container.append(response);
                            } else {
                                container.html(response);
                            }
                            // close and remove the modal html
                            $(modal_selector).find("button[data-dismiss='modal']").click();

                            if (modal_selector == '#tableCreateModal'){
                                that.init_tooltips();

                                var table_id_selector = "#" + $(response).attr("id");
                                that.make_draggable(table_id_selector, "#tablesWrap");
                                that.prettify_tbl_number($(table_id_selector));
                            }
                        } else {
                            // validation errors - json response
                            $.each(response, function(k, v){
                                var invalidFieldWrap = self.find($("[name=" + k + "]")).parent();
                                if (invalidFieldWrap.find(".errorlist").length == 0){
                                    invalidFieldWrap.append($("<ul/>").addClass("errorlist"));
                                }
                                $.each(v, function(index, item){
                                    invalidFieldWrap.find(".errorlist").html($("<li/>").text(item));
                                });
                            });
                        }
                        self.removeClass("submitting");
                    }
                });
            }
        });

        // close the modal - remove the modal
        $(modal_selector).find("button[data-dismiss='modal']").click(function(){
            $(modal_selector).modal("hide");

            setTimeout(function(){
                $(modal_selector).remove();
            }, 1000);
        });
    },
    // highlight: function(container, timeout){
    //     if (typeof(timeout) == 'undefined'){
    //         timeout = 2000;
    //     }
    //     container.addClass("highlighted");
    //     setTimeout(function(){
    //         container.removeClass("highlighted");
    //     }, timeout);
    // },
    send_alert: function(message, level){
        if ($("#messageWrap").length == 0){
            var wrap = $("<div/>").attr("id", "messageWrap").hide();
            $("body").append(wrap);
        }

        if (typeof(level) == 'undefined'){
            level = 'warning';
        }
        var container = $("<div/>").addClass("alert alert-" + level).html(message);
        $("#messageWrap").append(container).show();

        setTimeout(function(){
            $("#messageWrap").html("").hide();
        }, 5000);
    }
}

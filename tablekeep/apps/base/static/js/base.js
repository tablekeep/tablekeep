Dragon = {
    subscribe: {
        to_table: function(){
            // subscribe to table router when swampdragon is ready
            swampdragon.ready(function(){
                swampdragon.subscribe('table_router', 'table-channel', null, function(context, data){
                }, function(context, data){
                });
            });
        }
    },
    on_channel: function(channel){
        swampdragon.onChannelMessage(function(channels, message){
            // ...
        })
    },
}
Base = {
    init: function(){
        Utils.init_tooltips();
        Dragon.subscribe.to_table();
    },
}

$(function(){
    Base.init();
});

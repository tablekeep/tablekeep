from collections import OrderedDict

from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import PasswordChangeForm


class FunkyForm(object):
    """
    A funky form: add bootstrap classes to form fields.
    """
    def __init__(self, *args, **kwargs):
        super(FunkyForm, self).__init__(*args, **kwargs)
        for name, field in self.fields.items():
            field.widget.attrs.update({'class': 'form-control'})


class PasswordForm(FunkyForm, PasswordChangeForm):
    def __init__(self, *args, **kwargs):
        super(PasswordForm, self).__init__(*args, **kwargs)

        self.fields['old_password'].label = 'Parola veche'
        self.fields['new_password1'].label = 'Parola noua'
        self.fields['new_password2'].label = 'Confirmare parola noua'


PasswordForm.base_fields = OrderedDict(
    (k, PasswordChangeForm.base_fields[k])
    for k in ['old_password', 'new_password1', 'new_password2']
)


class UserForm(FunkyForm, forms.ModelForm):
    first_name = forms.CharField(label='Prenume')
    last_name = forms.CharField(label='Nume')
    email = forms.EmailField(label='Email')

    class Meta:
        model = User
        fields = ('first_name', 'last_name', 'email')

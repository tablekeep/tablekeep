import os
import sys
from fabric.api import env, run, sudo

HOSTS = {
    'live': {
        'branch': 'master',
        'hosts': ['bbox'],
        'path': '/home/django/tablekeep/venv/tablekeep',
        'user': 'butler',
        'venv_activate': '/home/django/tablekeep/venv/bin/activate',
    },
}

DEPLOYMENT = {
    #'migrate': 'python arges/manage.py migrate --merge',
    'collectstatic': 'python manage.py collectstatic --noinput',
    'restart_supervisor': 'sudo supervisorctl restart tablekeep tablekeep_swampdragon',
    'syncdb': 'python manage.py syncdb',
}

_key_files = ['~/.ssh/id_rsa']

_existing_keys = filter(os.path.exists, map(os.path.expanduser, _key_files))

if not _existing_keys:
    raise IOError('Can\'t find any of %s' % ', '.join(_key_files))
    env.key_filename = _existing_keys[0]


def host_setup(target):
    cfg = HOSTS[target]
    env.branch = cfg['branch']
    env.hosts = cfg['hosts']
    env.path = cfg['path']
    env.user = cfg['user']
    env.venv_activate = cfg['venv_activate']


def site_setup():
    cfg = DEPLOYMENT
    env.collectstatic = cfg['collectstatic']
    env.restart_supervisor = cfg['restart_supervisor']
    env.syncdb = cfg['syncdb']


def live():
    host_setup('live')


def update():
    site_setup()

    ##########################################################################
    # if revision:
    #     env.revision = revision
    #     commands = [
    #         "cd %(path)s/" % env,
    #         "git checkout %(revision)s" % env,
    #     ]
    #     run(';'.join(commands))
    ##########################################################################

    # sudo_commands = [
    #     "apt-get install `cat %(path)s/requirements/debian | xargs`" % env,
    # ]
    # sudo(';'.join(sudo_commands))

    project_commands = [
        'cd %(path)s/' % env,
        'git checkout %(branch)s' % env,
        'git pull origin %(branch)s' % env,
        'source %(venv_activate)s' % env,
        'pip install -r requirements/pip',
        '%(syncdb)s' % env,
        #'%(migrate)s' % env,
        '%(collectstatic)s' % env,
    ]
    run(';'.join(project_commands))
    run('%(restart_supervisor)s' % env)
